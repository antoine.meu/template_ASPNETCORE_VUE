
import RuleManager from 'components/rule-manager/rule-manager'
import ProjectBrowser from 'components/project-browser/project-browser'
import DashBoard from 'components/dashboard/dash-board'
import Reporting from 'components/reporting/reporting-tool'

//Test Stuff
//import CounterExample from 'components/test/counter-example'
//import FetchData from 'components/test/fetch-data'
//import HomePage from 'components/test/home-page'


export const routes = [
    { path: '/projects', component: ProjectBrowser},
    { path: '/rule-manager', component: RuleManager },
    { path: '/dashboard', component: DashBoard },
    { path: '/reporting', component: Reporting }

    //{ path: '/', component: HomePage },
    //{ path: '/counter', component: CounterExample },
    //{ path: '/fetch-data', component: FetchData},



]
